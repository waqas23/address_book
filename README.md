Project Approach
--------------

* I have used Service Oriented Architecture (with repositories layer) in the task while using Symfony 3.4 framework and SqlLite3.
* In my approach I have focused on SOLID principles, object oriented style and MVC as well as Dependency Injection Pattern.
* I write unit tests using PHPUnit 7.5

Setup Guide
--------------
Extract the zip file

Please run ```composer install``` to update vendors

Please run following command on the project root to start project

```php bin/console server:start```

Please run following command to run test cases

```./vendor/bin/phpunit src/ContactBundle```